extern crate includedir_codegen;

use includedir_codegen::Compression;

fn main() {
    includedir_codegen::start("RSRC")
        .dir("rsrc", Compression::Gzip)
        .build("rsrc.rs")
        .unwrap();
}