use super::{
    rng,
    world::{World, Room},
    gim,
    gim::CardsUnlocked,
    prize::Prize,
    resource::get_room_card_limits,
    mt19937ar::MT19937,
    types::EntityId
};

use std::collections::HashMap;
use std::fmt;
use strum_macros::EnumIter;
use strum::IntoEnumIterator;

pub enum Action {
    /// Note that rolling advances the rng by one and also performing
    /// a keyblade strike without breaking anything will have the same effect.
    Roll { count: usize },
    /// Used to signify attempting to get an item from a gimmick, an object
    /// that drops items.
    /// 
    /// The left value is the `EntityId` of the gimmick (object type) hit and
    /// the right is the number of hits it took to break the gimmick.
    Break { gimmick_id: EntityId, hit_count: usize },
}

/// An enumeration representing the playable characters in the game.
#[derive(EnumIter, Copy, Clone, Eq, PartialEq)]
pub enum Character {
    Sora,
    Riku,
}

impl fmt::Display for Character {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", match self {
            Self::Sora => "Sora",
            Self::Riku => "Riku"
        })
    }
}

#[derive(Clone)]
pub struct Predictor {
    mt: MT19937,
    cards_dropped: HashMap<Room, i16>,
    pub world: World,
    pub room: Room,
    pub cards_unlocked: CardsUnlocked,
    pub character: Character,
}

impl Predictor {
    pub fn new(world: World,
        room: Room,
        cards_unlocked: CardsUnlocked,
        character: Character
    ) -> Self {
        Predictor {
            mt: MT19937::from(rng::HD_SEED),
            cards_dropped: Self::initial_cards_dropped(),
            world,
            room,
            cards_unlocked,
            character
        }
    }

    pub fn get_cards_dropped(&self) -> &HashMap<Room, i16> {
        &self.cards_dropped
    }

    pub fn get_mt(&self) -> &MT19937 {
        &self.mt
    }

    fn initial_cards_dropped() -> HashMap<Room, i16> {
        let all = Room::iter();
        let mut cards_dropped = HashMap::with_capacity(all.len());

        for room in all {
            cards_dropped.insert(room, 0);
        }

        cards_dropped
    }

    pub fn set_rng_state(&mut self, advances: usize) {
        rng::set_rng_state(&mut self.mt, rng::HD_SEED, advances);
    }

    pub fn reset(&mut self) {
        self.mt.init_genrand(rng::HD_SEED);
        self.cards_dropped = Self::initial_cards_dropped();
    }

    pub fn predict(&mut self, action: &Action) -> Option<Prize> {
        let current_room = self.room;

        match action {
            Action::Roll { count } => {
                rng::advance_rng_state(&mut self.mt, *count);
                None
            },
            Action::Break { gimmick_id, hit_count } => {
                rng::advance_rng_state(&mut self.mt, *hit_count);

                let dropped = self.cards_dropped[&current_room];

                let card_limit_reached =
                    dropped > get_room_card_limits()[&current_room];

                let result = gim::decide_prize(
                    &mut self.mt,
                    self.world,
                    *gimmick_id,
                    &self.cards_unlocked,
                    match self.character {
                        Character::Sora => true,
                        Character::Riku => false
                    },
                    card_limit_reached
                );

                if let Some(result) = result.as_ref() {
                    if let Prize::Card(_) = result {
                        self.cards_dropped.insert(current_room, dropped + 1);
                    }
                }

                result
            }
        }
    }

    /// Predicts the prizes that will be obtained by perfoming a set of actions
    /// in the environment specified by the fields in `Predictor`.
    /// 
    /// There is one result per action in `actions`.
    pub fn predict_all(&mut self, actions: &[Action]) -> Vec<Option<Prize>> {
        actions.iter().map(|a| self.predict(a)).collect()
    }

    /// Predicts the prizes that will be obtained by perfoming a set of actions
    /// in the environment specified by the fields in `Predictor`.
    /// 
    /// Filters out `None` results.
    pub fn predict_all_filter(&mut self, actions: &[Action]) -> Vec<Prize> {
        self.predict_all(&actions).into_iter().filter_map(|x| x).collect()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use super::super::{
        prize::{Prize, Orbs, OrbSize},
        card::Card,
        gim::{Gimmick, UnlockableCard}
    };

    #[test]
    fn test_predictor() {
        // Test the first part of the RNG manipulation route for Traverse Town
        let mut pred = Predictor::new(
            World::TraverseTown,
            Room::MomentsReprieve,
            UnlockableCard::none(),
            Character::Sora
        );

        let actions = vec![
            Action::Roll { count: 6 },
            Action::Break { gimmick_id: Gimmick::TROrangeLamp.id(), hit_count: 1 },
            Action::Roll { count: 7 },
            Action::Break { gimmick_id: Gimmick::TRCrate.id(), hit_count: 1 },
            Action::Break { gimmick_id: Gimmick::TRCrate.id(), hit_count: 0 },
            Action::Roll { count: 3 },
            Action::Break { gimmick_id: Gimmick::TROrangeLamp.id(), hit_count: 1 },
            Action::Roll { count: 4 },
            Action::Break { gimmick_id: Gimmick::TRLampPost.id(), hit_count: 1 },
        ];

        let results = pred.predict_all(&actions);
        pred.reset();
        let filtered_results = pred.predict_all_filter(&actions);
        let correct_results = vec![
            None,
            Some(Prize::PrizeOrbs(Orbs::MP { size: OrbSize::Small, amount: 3 })),
            None,
            Some(Prize::PrizeOrbs(Orbs::MP { size: OrbSize::Large, amount: 5 })),
            Some(Prize::PrizeOrbs(Orbs::MP { size: OrbSize::Small, amount: 5 })),
            None,
            Some(Prize::PrizeOrbs(Orbs::MP { size: OrbSize::Large, amount: 5 })),
            None,
            Some(Prize::Card(Card::new(3, false, 0x1B))),
        ];

        assert_eq!(results, correct_results);
        assert_eq!(filtered_results, correct_results.iter()
            .cloned()
            .filter_map(|x| x)
            .collect::<Vec<_>>()
        );
    }
}
