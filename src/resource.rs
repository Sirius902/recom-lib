use std::{
    io::{Cursor, Read},
    collections::HashMap
};

use byteorder::{LE, ReadBytesExt};
use super::{
    world::{World, Room},
    types::EntityId
};

use strum::IntoEnumIterator;

include!(concat!(env!("OUT_DIR"), "/rsrc.rs"));

pub fn get_prize_gim() -> [i16; 10] {
    let gim_com = RSRC.get("rsrc/GimCom.bin").unwrap();
    let mut file = Cursor::new(gim_com);
    // Skip header
    file.set_position(6);

    let mut values = [0; 10];
    file.read_i16_into::<LE>(&mut values).unwrap();

    values
}

pub fn get_card_limit(room: Room) -> Option<i16> {
    let scene_number = room.scene_number();
    let gim_com = RSRC.get("rsrc/GimCom.bin").unwrap();
    let mut file = Cursor::new(gim_com);
    // Skip header and card prize gim data
    file.set_position(6 + 2 * 10);

    loop {
        let current_scene = file.read_i16::<LE>();
        let current_card_limit = file.read_i16::<LE>();

        if let Ok(current_scene) = current_scene {
            if let Ok(current_card_limit) = current_card_limit {
                if current_scene == scene_number {
                    return Some(current_card_limit);
                }
            }
        } else {
            break;
        }
    }

    None
}

pub fn get_room_card_limits() -> HashMap<Room, i16> {
    let all = Room::iter();
    let mut hm = HashMap::with_capacity(all.len());

    for room in all {
        hm.insert(room, get_card_limit(room).unwrap());
    }

    hm
}

/// Performs first part of function LoadWorldResrc__15CGimmickManager, starting at 0x002E4CF4
pub fn load_every_card_prob(world: World) -> Vec<i16> {
    let atk_card_drop = RSRC.get(&format!("rsrc/world/{}/AtkCardDrop.bin", world.dir_name())).unwrap();
    let mut file = Cursor::new(atk_card_drop);
    // Skip header
    file.set_position(6);

    let len = file.read_i16::<LE>().unwrap();
    let mut v = vec![];

    for _ in 0..len * 2 {
        v.push(file.read_i16::<LE>().unwrap());
    }

    v
}

// TODO: Find out what this is
/// Performs second part of function LoadWorldResrc__15CGimmickManager, starting at 0x002E4DB4
pub fn load_unk_world_resource(world: World) -> Vec<i16> {
    let atk_card_drop = RSRC.get(&format!("rsrc/world/{}/AtkCardDrop.bin", world.dir_name())).unwrap();
    let mut file = Cursor::new(atk_card_drop);
    // Skip header
    file.set_position(6);

    {
        // Skip first part of file
        let len = file.read_i16::<LE>().unwrap();
        file.set_position(6 + len as u64 * 4 + 2);
    }

    let len = file.read_i16::<LE>().unwrap();
    let mut v = vec![];

    for _ in 0..len * 2 {
        v.push(file.read_i16::<LE>().unwrap());
    }

    v
}

/// Performs last part of function LoadWorldResrc__15CGimmickManager, starting at 0x002E4E28
pub fn load_gim_prob(world: World) -> HashMap<EntityId, [u8; 14]> {
    let gim_prob = RSRC.get(&format!("rsrc/world/{}/GimProb.bin", world.dir_name())).unwrap();
    let mut file = Cursor::new(gim_prob);

    let mut hm = HashMap::new();
    let len = file.read_u16::<LE>().unwrap();

    for _ in 0..len {
        let key = file.read_u16::<LE>().unwrap();
        let mut buf = [0; 12];
        file.read_exact(&mut buf).unwrap();

        let mut value = [0; 14];
        value[0] = (key & 0xFF) as u8;
        value[1] = ((key >> 8) & 0xFF) as u8;

        value[2..5].clone_from_slice(&buf[0..(5 - 2)]);

        value[6] = buf[9];

        value[7..12].clone_from_slice(&buf[(7 - 3)..(12 - 3)]);
        value[12..14].clone_from_slice(&buf[(12 - 2)..(14 - 2)]);

        hm.insert(key, value);
    }

    hm
}

pub fn hp_drop_prize_table(world: World) -> Vec<u8> {
    let prize_table = RSRC.get(&format!("rsrc/world/{}/GimProbTable.bin", world.dir_name())).unwrap();
    let mut file = Cursor::new(prize_table);

    let hp_table_entries = file.read_u8().unwrap() as usize;
    let hp_table_size = hp_table_entries as usize * 0x12;

    file.set_position(file.position() + 1);

    let mut result = Vec::with_capacity(hp_table_size as usize);
    for _ in 0..hp_table_size { result.push(0); }

    for i in 0..hp_table_entries {
        for j in 0..6 {
            result[i * 0x12 + j] = file.read_u8().unwrap();
            result[i * 0x12 + j + 6] = file.read_u8().unwrap();
            result[i * 0x12 + j + 12] = file.read_u8().unwrap();
        }
    }

    result
}

pub fn mp_drop_prize_table(world: World) -> Vec<u8> {
    let prize_table = RSRC.get(&format!("rsrc/world/{}/GimProbTable.bin", world.dir_name())).unwrap();
    let mut file = Cursor::new(prize_table);

    let hp_table_entries = file.read_u8().unwrap() as usize;
    let hp_table_size = hp_table_entries as usize * 0x12;

    let mp_table_entries = file.read_u8().unwrap() as usize;
    let mp_table_size = mp_table_entries as usize * 0x12;

    file.set_position(file.position() + hp_table_size as u64);

    let mut result = Vec::with_capacity(mp_table_size as usize);
    for _ in 0..mp_table_size { result.push(0); }

    for i in 0..mp_table_entries {
        for j in 0..6 {
            result[i * 0x12 + j] = file.read_u8().unwrap();
            result[i * 0x12 + j + 6] = file.read_u8().unwrap();
            result[i * 0x12 + j + 12] = file.read_u8().unwrap();
        }
    }

    result
}

pub fn get_battle_card_list() -> Option<Vec<u8>> {
    RSRC.get("rsrc/BattleCardList.bin").map(|bytes| bytes.to_vec()).ok()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_hp_drop_prize_table() {
        assert_eq!(hp_drop_prize_table(World::TraverseTown), vec![ 0x00, 0x00, 0x00, 0x01, 0x01, 0x01, 0x0A, 0x0F, 0x1E, 0x0A, 0x14, 0x0F, 0x01, 0x03, 0x05, 0x01, 0x03, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1E, 0x14, 0x14, 0x0F, 0x0A, 0x05, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x00, 0x00, 0x00, 0x01, 0x01, 0x01, 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x02, 0x03, 0x06, 0x02, 0x03 ]);
        assert_eq!(hp_drop_prize_table(World::CastleOblivion), vec![ 0x00, 0x00, 0x00, 0x01, 0x01, 0x01, 0x0A, 0x1E, 0x14, 0x0A, 0x14, 0x0A, 0x01, 0x03, 0x05, 0x01, 0x03, 0x05, 0x00, 0x00, 0x00, 0x01, 0x01, 0x01, 0x05, 0x14, 0x14, 0x05, 0x1E, 0x14, 0x01, 0x03, 0x05, 0x01, 0x03, 0x05, 0x00, 0x00, 0x00, 0x01, 0x01, 0x01, 0x00, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x02, 0x03, 0x06, 0x02, 0x03, 0x00, 0x00, 0x00, 0x01, 0x01, 0x01, 0x50, 0x00, 0x00, 0x14, 0x00, 0x00, 0x02, 0x00, 0x00, 0x01, 0x00, 0x00 ]);
    }

    #[test]
    fn test_mp_drop_prize_table() {
        assert_eq!(mp_drop_prize_table(World::TraverseTown), vec![ 0x00, 0x00, 0x00, 0x01, 0x01, 0x01, 0x0A, 0x0A, 0x1E, 0x0A, 0x0A, 0x1E, 0x01, 0x03, 0x05, 0x01, 0x03, 0x05, 0x00, 0x00, 0x00, 0x01, 0x01, 0x01, 0x1E, 0x14, 0x14, 0x0F, 0x0A, 0x05, 0x02, 0x03, 0x05, 0x06, 0x08, 0x0A ]);
        assert_eq!(mp_drop_prize_table(World::Agrabah), vec![ 0x00, 0x00, 0x00, 0x01, 0x01, 0x01, 0x0A, 0x1E, 0x0A, 0x0A, 0x1E, 0x0A, 0x01, 0x03, 0x05, 0x01, 0x03, 0x05, 0x00, 0x00, 0x00, 0x01, 0x01, 0x01, 0x0A, 0x0A, 0x1E, 0x0A, 0x0A, 0x1E, 0x01, 0x03, 0x05, 0x01, 0x03, 0x05 ]);
        assert_eq!(mp_drop_prize_table(World::CastleOblivion), vec![ 0x00, 0x00, 0x00, 0x01, 0x01, 0x01, 0x0A, 0x1E, 0x0A, 0x0A, 0x1E, 0x0A, 0x01, 0x03, 0x05, 0x01, 0x03, 0x05, 0x00, 0x00, 0x00, 0x01, 0x01, 0x01, 0x0A, 0x0A, 0x1E, 0x0A, 0x0A, 0x1E, 0x01, 0x03, 0x05, 0x01, 0x03, 0x05, 0x00, 0x00, 0x00, 0x01, 0x01, 0x01, 0x50, 0x00, 0x00, 0x14, 0x00, 0x00, 0x03, 0x00, 0x00, 0x02, 0x00, 0x00 ]);
    }
}
