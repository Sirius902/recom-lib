use super::{
    rng,
    world::World,
    resource,
    prize::{Prize, Orbs, OrbSize},
    card::Card,
    mt19937ar::MT19937,
    types::EntityId
};

mod gimmick;
mod unlockable_card;

pub use gimmick::Gimmick;
pub use unlockable_card::{UnlockableCard, CardsUnlocked};

mod map_cards;

use std::convert::{TryFrom, TryInto};

#[derive(Debug, Eq, PartialEq)]
pub enum PrizeKind {
    None,
    HP,
    MP,
    Card,
    Mimic
}

impl TryFrom<u8> for PrizeKind {
    type Error = &'static str;

    fn try_from(kind: u8) -> Result<Self, Self::Error> {
        match kind {
            0x0 => Ok(PrizeKind::None),
            0x1 => Ok(PrizeKind::HP),
            0x2 => Ok(PrizeKind::MP),
            0x3 => Ok(PrizeKind::Card),
            0x4 => Ok(PrizeKind::Mimic),
            _ => Err("Invalid prize kind!")
        }
    }
}

/// Advances the RNG as the game would when spawning a prize object.
pub fn create_prize(mt: &mut MT19937, prize: &Prize) {
    match prize {
        Prize::PrizeOrbs(orbs) => {
            match orbs {
                Orbs::HP { amount, .. } | Orbs::MP { amount, .. } => {
                    for _ in 0..*amount {
                        let _ = mt.genrand_int31();
                        let _ = mt.genrand_int31();
                        let _ = rng::genrand_real1(mt);
                    }
                } 
            }
        },
        Prize::Card(_) => {
            let _ = mt.genrand_int31();
            let _ = mt.genrand_int31();
            let _ = rng::genrand_real1(mt);
        },
        _ => {}
    }
}

/// `card_limit_reached` should be `true` if the maximum number of cards for a given room has been generated
pub fn decide_prize(
    mt: &mut MT19937,
    world: World,
    gimmick_id: EntityId,
    cards_unlocked: &CardsUnlocked,
    is_sora: bool,
    card_limit_reached: bool
) -> Option<Prize> {
    let gim_param = resource::load_gim_prob(world)[&gimmick_id];

    // TODO: I'm not sure if the below code branch will ever run. Check!
    //
    // if 0xd < getCurrentWorldID(0) && getCurrentWorldID(0) != 0x17 || &gim_param == 0x0 { createPrize(this,(DropPrize *)&stack0xffffff50,0,param_2,param_3); return (DropPrize *)0x0; }

    let prize_kind = decide_prize_kind(mt, gim_param, is_sora, card_limit_reached);
    let result;

    match prize_kind {
        PrizeKind::Card => {
            let every_card_prob = resource::load_every_card_prob(world);
            let gim_com = resource::get_prize_gim();

            let check = decide_card_check(mt, &every_card_prob, gim_com);

            let prize_is_valid = check.as_ref().map(|p| {
                if let Prize::Card(card) = p {
                    check_card_possible(card.id, &cards_unlocked)
                } else {
                    false
                }
            }).contains(&true);

            result = Some(if prize_is_valid {
                check.unwrap()
            } else {
                decide_card_no_check(mt, &every_card_prob, gim_com)
            });
        },
        PrizeKind::HP => {
            result = Some(decide_g_prize(mt, gim_param[7], resource::hp_drop_prize_table(world)));
        },
        PrizeKind::MP => {
            result = Some(decide_r_prize(mt, gim_param[8], resource::mp_drop_prize_table(world)));
        },
        _ => { result = None; }
    }

    if let Some(result) = result.as_ref() {
        create_prize(mt, result);
    }

    result
}

pub fn decide_g_prize(mt: &mut MT19937, gim_param_x7: u8, floor_hp_drop_table: Vec<u8>) -> Prize {
    let mut remainder = mt.genrand_int31() % 100;
    let i_var5 = gim_param_x7 as usize * 0x12;
    let mut id = 0;
    let mut amount = 0;

    for i in 0..6 {
        let b_var1 = floor_hp_drop_table[i_var5 + i + 6];
        if remainder < i32::from(b_var1) {
            if floor_hp_drop_table[i_var5 + i] == 0 {
                id = 0xD3;
            } else {
                id = 0xD2;
            }
            amount = floor_hp_drop_table[i_var5 + i + 0xC];
            break;
        }

        remainder -= i32::from(b_var1);
    }

    match id {
        0xD3 => Prize::PrizeOrbs(Orbs::HP { size: OrbSize::Large, amount: amount as usize }),
        0xD2 => Prize::PrizeOrbs(Orbs::HP { size: OrbSize::Small, amount: amount as usize }),
        _ => panic!("Decide G Prize generated something other than HP orbs!")
    }
}

pub fn decide_r_prize(mt: &mut MT19937, gim_param_x8: u8, floor_mp_drop_table: Vec<u8>) -> Prize {
    let mut remainder = mt.genrand_int31() % 100;
    let i_var5 = gim_param_x8 as usize * 0x12;
    let mut id = 0;
    let mut amount = 0;

    for i in 0..6 {
        let b_var1 = floor_mp_drop_table[i_var5 + i + 6];
        if remainder < i32::from(b_var1) {
            if floor_mp_drop_table[i_var5 + i] == 0 {
                id = 0xD5;
            } else {
                id = 0xD4;
            }
            amount = floor_mp_drop_table[i_var5 + i + 0xC];
            break;
        }

        remainder -= i32::from(b_var1);
    }

    match id {
        0xD5 => Prize::PrizeOrbs(Orbs::MP { size: OrbSize::Large, amount: amount as usize }),
        0xD4 => Prize::PrizeOrbs(Orbs::MP { size: OrbSize::Small, amount: amount as usize }),
        _ => panic!("Decide R Prize generated something other than MP orbs!")
    }
}

pub fn decide_card_number(mt: &mut MT19937, gim_com: [i16; 10]) -> i16 {
    let mut i = 0;
    let mut remainder = mt.genrand_int31() % 100;

    loop {
        let current = i32::from(gim_com[i]);
        if remainder < current {
            return i as i16;
        }
        remainder -= current;
        i += 1;

        if i >= 10 {
            break;
        }
    }

    -1
}

pub fn decide_card_no_check(mt: &mut MT19937, every_card_prob: &[i16], gim_com: [i16; 10]) -> Prize {
    let param_1 = every_card_prob.len() / 2;
    let mut remainder = mt.genrand_int31() % 100;

    let mut card_id = 0;
    let mut card_value = 0;

    for i in 0..param_1 {
        let a0 = every_card_prob[i * 2];
        let a1;
        if a0 < 0x2A {
            match a0 {
                0x0 | 0x1 | 0x2 | 0x3 | 0x4 | 0x5 | 0x8 | 0xA | 0xB | 0x1A | 0x1C | 0x29 => {
                    a1 = every_card_prob[i * 2 + 1];

                    if remainder < i32::from(a1) {
                        card_id = a0 + 1;
                        card_value = decide_card_number(mt, gim_com);
                        break;
                    }

                    remainder -= i32::from(a1);
                },
                _ => {}
            }
        }
    }

    Prize::Card(
        Card::new(
            card_value.try_into().unwrap(),
            false,
            card_id.try_into().unwrap()
        )
    )
}

pub fn decide_card_check(mt: &mut MT19937, every_card_prob: &[i16], gim_com: [i16; 10]) -> Option<Prize> {
    let param_1 = every_card_prob.len() / 2;
    let mut remainder = mt.genrand_int31() % 100;

    let mut card_id = 0;
    let mut card_value = 0;

    for i in 0..param_1 {
        let a0 = every_card_prob[i * 2];
        let a1;
        if a0 < 0x2A {
            match a0 {
                0x0 | 0x1 | 0x2 | 0x3 | 0x4 | 0x5 | 0x8 | 0xA | 0xB | 0x1A | 0x1C | 0x29 => {},
                _ => {
                    a1 = every_card_prob[i * 2 + 1];

                    if remainder < i32::from(a1) {
                        card_id = a0 + 1;
                        card_value = decide_card_number(mt, gim_com);
                        break;
                    }

                    remainder -= i32::from(a1);
                }
            }
        }
    }

    if card_id == 0 {
        None
    } else {
        Some(
            Prize::Card(
                Card::new(
                    card_value.try_into().unwrap(),
                    false,
                    card_id.try_into().unwrap()
                )
            )
        )
    }
}

pub fn check_card_possible(id: u8, cards_unlocked: &CardsUnlocked) -> bool {
    if id == 0x00 {
        false
    } else {
        let card = UnlockableCard::try_from(id);
        cards_unlocked.contains(&card.expect("Invalid card id in check_card_possible!"))
    }
}

/// card_limit_reached should be true if the maximum number of cards for a given room has been generated
pub fn decide_prize_kind(
    mt: &mut MT19937,
    gim_param: [u8; 14],
    is_sora: bool,
    card_limit_reached: bool
) -> PrizeKind {
    let remainder = mt.genrand_int31() % 100;
    let u_var3 = gim_param[0xB];

    if is_sora {
        if remainder < i32::from(gim_param[0xB]) && !card_limit_reached {
            return PrizeKind::Card;
        }

        if (remainder - i32::from(u_var3)) < i32::from(gim_param[9]) {
            return PrizeKind::HP;
        }
        let i_var4 = remainder - i32::from(u_var3) - i32::from(gim_param[9]);
        if i_var4 < i32::from(gim_param[10]) {
            return PrizeKind::MP;
        }
        if (i_var4 - i32::from(gim_param[10])) < i32::from(gim_param[6]) {
            return PrizeKind::Mimic;
        }
    } else {
        if remainder < i32::from(gim_param[0xD]) {
            return PrizeKind::HP;
        }
        if (remainder - i32::from(gim_param[0xD])) < i32::from(gim_param[6]) {
            return PrizeKind::Mimic;
        }
    }

    PrizeKind::None
}

#[cfg(test)]
mod tests {
    use super::*;

    use super::super::{
        rng,
        resource,
        world::World
    };

    const EXPECTED: [u8; 50] = [
        0, 3, 3, 0, 1, 1, 1, 2, 2, 0, 
        2, 2, 3, 1, 2, 0, 2, 1, 1, 1, 
        1, 0, 0, 3, 1, 1, 2, 1, 0, 2, 
        0, 0, 1, 1, 2, 2, 2, 1, 1, 1, 
        3, 0, 0, 1, 0, 1, 3, 0, 2, 0
    ];

    #[test]
    fn test_decide_prize_kind() {
        let mut mt = MT19937::from(rng::HD_SEED);

        let gim_param = resource::load_gim_prob(World::Agrabah)[&Gimmick::AGPot.id()];

        for i in 0..50 {
            let prize_kind = decide_prize_kind(&mut mt, gim_param, true, false);
            let exp = PrizeKind::try_from(EXPECTED[i]).unwrap();
            assert_eq!(exp, prize_kind);
        }
    }

    #[test]
    fn test_decide_prize() {
        let mut mt = MT19937::uninitialized();
        rng::set_rng_state(&mut mt, rng::HD_SEED, 0x008);

        let world = World::TraverseTown;
        let gimmick = Gimmick::TRLampPost.id();
        let cards_unlocked = UnlockableCard::none();
        let is_sora = true;
        let card_limit_reached = false;

        let prize = decide_prize(&mut mt, world, gimmick, &cards_unlocked, is_sora, card_limit_reached);

        assert_eq!(prize, Some(Prize::PrizeOrbs(Orbs::MP { size: OrbSize::Small, amount: 5 })), "Failure to create correct prize!");
    }
}
