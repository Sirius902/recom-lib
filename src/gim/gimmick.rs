use super::super::types::EntityId;

// TODO: Add all gimmick types and add way to get possible gimmicks for World and move to different file
// Gimmick ids can be found by breakpointing DecidePrize__15CGimmickManageriPC7FVECTORT2i in parameter a1
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum Gimmick {
    // Traverse Town gimmicks
    TROrangeLamp,
    TRYellowLamp,
    TRLampPost,
    TRDirectionPost,
    TRBarrel,
    TRCrate,
    TRBanner,
    TRGreenLamp,
    TRHatMailbox,
    TRMailbox,
    TRRedBannerPost,
    // Agrabah gimmicks
    AGPot,
    AGBarrel,
    AGPinkMelonTent,
    AGBlueGrainTent,
    AGSilverRugTent,
    AGPurplePotteryTent,
    AGPurplePotteryTentSmall,
    AGRedGrainTent,
    AGDarkRedSackTent,
    AGPileOfCrates,
    AGSilverAppleTent,
    AGWoodenStructure,
    AGGreenRugTent,
    // Castle Oblivion Gimmicks
    CARoseBlock
}

impl Gimmick {
    /// This is the entity id for the gimmick, gimmick_id is *(int*)(this: CBaseGimmick* + 0xBC), essentially it is the id of the object type hit
    /// 
    /// `gimmick_id % 10000 = spawn_id`
    pub fn id(self) -> EntityId {
        match self {
            Self::TROrangeLamp => 0x6D,
            Self::TRYellowLamp => 0xA1,
            Self::TRLampPost => 0x68,
            Self::TRDirectionPost => 0x69,
            Self::TRBarrel => 0x85,
            Self::TRGreenLamp => 0x6A,
            Self::TRMailbox => 0x6C,
            Self::TRHatMailbox => 0x6B,
            Self::TRRedBannerPost => 0xA0,
            Self::TRCrate => 0x84,
            Self::TRBanner => 0xA3,
            Self::AGPot => 0xE2,
            Self::AGBarrel => 0xE6,
            Self::AGPinkMelonTent => 0xD7,
            Self::AGBlueGrainTent => 0xD4,
            Self::AGSilverRugTent => 0xD5,
            Self::AGPurplePotteryTent => 0xD3,
            Self::AGPurplePotteryTentSmall => 0xDE,
            Self::AGRedGrainTent => 0xDD,
            Self::AGDarkRedSackTent => 0xD6,
            Self::AGPileOfCrates => 0xDF,
            Self::AGSilverAppleTent => 0xDA,
            Self::AGWoodenStructure => 0xD2,
            Self::AGGreenRugTent => 0xD9,
            Self::CARoseBlock => 0x51D
        }
    }
}
