use std::collections::HashSet;
use std::convert::TryFrom;
use std::fmt;
use strum::IntoEnumIterator;
use strum_macros::EnumIter;
use crate::card::CARD_DATA_TABLE;
use crate::card::CardCategory;

pub type CardsUnlocked = HashSet<UnlockableCard>;

/// A list of all the cards that are unlockable in recom.
#[derive(Copy, Clone, Hash, Eq, PartialEq, EnumIter)]
pub enum UnlockableCard {
    Spellbinder,
    MetalChocobo,
    Lionheart,
    DivineRose,
    Oathkeeper,
    Oblivion,
    UltimaWeapon,
    DiamondDust,
    OneWingedAngel,
    StarSeeker,
    Monochrome,
    FollowTheWind,
    HiddenDragon,
    PhotonDebugger,
    BondOfFlame,
    Fire,
    Thunder,
    Gravity,
    Stop,
    Aero,
    Simba,
    Genie,
    Bambi,
    Dumbo,
    TinkerBell,
    Mushu,
    Cloud,
    HiPotion,
    MegaPotion,
    Ether,
    MegaEther,
    Elixir,
    Megalixir
}

impl UnlockableCard {
    pub fn none() -> HashSet<Self> {
        HashSet::new()
    }

    pub fn all() -> HashSet<Self> {
        Self::iter().collect::<HashSet<_>>()
    }

    pub fn category(self) -> CardCategory {
        let entry = &CARD_DATA_TABLE[u8::try_from(self).unwrap() as usize];
        CardCategory::try_from(entry.category).unwrap()
    }
}

impl From<UnlockableCard> for u8 {
    fn from(card: UnlockableCard) -> u8 {
        match card {
            UnlockableCard::Spellbinder => 0x07,
            UnlockableCard::MetalChocobo => 0x08,
            UnlockableCard::Lionheart => 0x0A,
            UnlockableCard::DivineRose => 0x0C,
            UnlockableCard::Oathkeeper => 0x0D,
            UnlockableCard::Oblivion => 0x0E,
            UnlockableCard::UltimaWeapon => 0x0F,
            UnlockableCard::DiamondDust => 0x10,
            UnlockableCard::OneWingedAngel => 0x11,
            UnlockableCard::StarSeeker => 0x13,
            UnlockableCard::Monochrome => 0x14,
            UnlockableCard::FollowTheWind => 0x15,
            UnlockableCard::HiddenDragon => 0x16,
            UnlockableCard::PhotonDebugger => 0x17,
            UnlockableCard::BondOfFlame => 0x18,
            UnlockableCard::Fire => 0x1A,
            UnlockableCard::Thunder => 0x1C,
            UnlockableCard::Gravity => 0x1E,
            UnlockableCard::Stop => 0x1F,
            UnlockableCard::Aero => 0x20,
            UnlockableCard::Simba => 0x23,
            UnlockableCard::Genie => 0x24,
            UnlockableCard::Bambi => 0x25,
            UnlockableCard::Dumbo => 0x26,
            UnlockableCard::TinkerBell => 0x27,
            UnlockableCard::Mushu => 0x28,
            UnlockableCard::Cloud => 0x29,
            UnlockableCard::HiPotion => 0x2B,
            UnlockableCard::MegaPotion => 0x2C,
            UnlockableCard::Ether => 0x2D,
            UnlockableCard::MegaEther => 0x2E,
            UnlockableCard::Elixir => 0x2F,
            UnlockableCard::Megalixir => 0x30
        }
    }
}

impl TryFrom<u8> for UnlockableCard {
    type Error = &'static str;

    fn try_from(id: u8) -> Result<Self, Self::Error> {
        match id {
            0x07 => Ok(Self::Spellbinder),
            0x08 => Ok(Self::MetalChocobo),
            0x0A => Ok(Self::Lionheart),
            0x0C => Ok(Self::DivineRose),
            0x0D => Ok(Self::Oathkeeper),
            0x0E => Ok(Self::Oblivion),
            0x0F => Ok(Self::UltimaWeapon),
            0x10 => Ok(Self::DiamondDust),
            0x11 => Ok(Self::OneWingedAngel),
            0x13 => Ok(Self::StarSeeker),
            0x14 => Ok(Self::Monochrome),
            0x15 => Ok(Self::FollowTheWind),
            0x16 => Ok(Self::HiddenDragon),
            0x17 => Ok(Self::PhotonDebugger),
            0x18 => Ok(Self::BondOfFlame),
            0x1A => Ok(Self::Fire),
            0x1C => Ok(Self::Thunder),
            0x1E => Ok(Self::Gravity),
            0x1F => Ok(Self::Stop),
            0x20 => Ok(Self::Aero),
            0x23 => Ok(Self::Simba),
            0x24 => Ok(Self::Genie),
            0x25 => Ok(Self::Bambi),
            0x26 => Ok(Self::Dumbo),
            0x27 => Ok(Self::TinkerBell),
            0x28 => Ok(Self::Mushu),
            0x29 => Ok(Self::Cloud),
            0x2B => Ok(Self::HiPotion),
            0x2C => Ok(Self::MegaPotion),
            0x2D => Ok(Self::Ether),
            0x2E => Ok(Self::MegaEther),
            0x2F => Ok(Self::Elixir),
            0x30 => Ok(Self::Megalixir),
            _ => Err("Invalid card id!")
        }
    }
}

impl fmt::Display for UnlockableCard {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", CARD_DATA_TABLE[usize::from(u8::from(*self))].name)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn display_test() {
        assert_eq!(UnlockableCard::BondOfFlame.to_string(), "Bond of Flame".to_owned());
    }

    #[test]
    fn category_test() {
        let cs = [CardCategory::AttackCard, CardCategory::MagicCard, CardCategory::SummonCard, CardCategory::ItemCard];

        assert_eq!(UnlockableCard::DivineRose.category(), CardCategory::AttackCard);
        assert_eq!(UnlockableCard::Aero.category(), CardCategory::MagicCard);
        assert_eq!(UnlockableCard::MegaEther.category(), CardCategory::ItemCard);

        UnlockableCard::all()
            .into_iter()
            .for_each(|c| {
                let valid_category = cs.contains(&c.category());
                assert!(valid_category, format!("Card {} has an unexpected category!", c));
            });
    }
}
