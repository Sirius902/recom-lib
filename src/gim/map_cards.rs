#[allow(unused_imports)]
use super::{
    rng,
    super::world::World,
    resource,
    super::prize::Prize,
    super::mt19937ar::MT19937
};

// CBattleEnemy::makeCard(FVECTOR const*)
#[allow(dead_code)]
pub fn make_map_card(_mt: &mut MT19937) {
    unimplemented!()
}

/// When called by another function `param_1` should be `1`
#[allow(dead_code)]
pub fn decide_map_card_prize(mt: &mut MT19937, param_1: i32) -> Prize {
    let rand = mt.genrand_int31();
    let _remainder = rand % 10000;

    // if *(int*)(this + 0x104) != 0

    if param_1 == 1 {
        // local_80 = getByte(0, 0x61) # Last time I checked this returned 2. TODO: Verify
        let _local_80 = 2;

    }

    unimplemented!()
}

#[allow(dead_code, unused_variables)]
pub fn decide_map_card(
    mt: &mut MT19937,
    param_1: i32,
    every_card_prob: &[i16], // Seems to be different than the other every_card_prob
                                // *(GimmickEveryCardProb**)(CGimmickManager + 0x104)
    param_3: &[i16] // (short*)(CGimmickManager + 0xE8)
) -> Prize {
    unimplemented!()
}
