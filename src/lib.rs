#![feature(option_result_contains)]

extern crate mt19937ar;
extern crate byteorder;
extern crate strum;
extern crate strum_macros;

pub mod resource;
pub mod card;
pub mod rng;
pub mod gim;
pub mod world;
pub mod prize;
pub mod predictor;
pub mod types;
