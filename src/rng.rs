use mt19937ar::MT19937;

/// The seed for the RNG in Re: Chain of Memories in HD 1.5 Remix
pub const HD_SEED: u32 = 0x0009FBF1;

/// generates a random number on [0,1]-real-interval
/// 
/// This version of the function returns an `f32` because it does in recom.
pub fn genrand_real1(mt: &mut MT19937) -> f32 {
    (mt.genrand_int32() as f32) * (1.0 / 4294967295.0)
}

/// generates a random number on [0,1)-real-interval
/// 
/// This version of the function returns an `f32` because it does in recom.
pub fn genrand_real2(mt: &mut MT19937) -> f32 {
    (mt.genrand_int32() as f32) * (1.0 / 4294967296.0)
}

/// Reseeds the generator and dvances the state of the generator `advances` times.
pub fn set_rng_state(mt: &mut MT19937, s: u32, advances: usize) {
    mt.init_genrand(s);

    advance_rng_state(mt, advances);
}

pub fn advance_rng_state(mt: &mut MT19937, advances: usize) {
    for _ in 0..advances {
        let _ = mt.genrand_int32();
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use super::super::mt19937ar::N;

    #[test]
    fn test_set_rng_state() {
        let mut mt = MT19937::from(HD_SEED);
        let r = mt.genrand_int32();

        set_rng_state(&mut mt, HD_SEED, 0);
        assert_eq!(r, mt.genrand_int32());

        mt.init_genrand(HD_SEED);

        for _ in 0..(N * 2 - 1) {
            let _ = mt.genrand_int32();
        }

        let r = mt.genrand_int32();

        set_rng_state(&mut mt, HD_SEED, N * 2 - 1);
        assert_eq!(r, mt.genrand_int32());
    }
}
