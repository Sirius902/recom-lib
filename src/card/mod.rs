mod data_table;
mod category;

pub use data_table::CARD_DATA_TABLE;
pub use category::CardCategory;

use std::fmt;
use std::convert::TryFrom;

use super::{resource as rs};

#[derive(Copy, Clone, PartialEq, Eq)]
pub struct Card {
    pub value: u8,
    pub is_premium: bool,
    pub id: u8
}

impl Card {
    /// `id` must be less than `CARD_DATA_TABLE.len()`
    pub const fn new(value: u8, is_premium: bool, id: u8) -> Self {
        Card { value, is_premium, id }
    }

    pub fn get_cp(self) -> u32 {
        let card = u32::from(self);
        let mut cp = 0;

        if (card - 1) & 0xFF < 0x7E {
            let b_var1 = rs::get_battle_card_list().unwrap()[((card & 0xFF) * 0x10 + 10) as usize];
            cp = u32::from(b_var1);
            if (card >> 8).trailing_zeros() >= 8 && (card & 0xFF) < 0x44 {
                let mut i_var3 = ((card << 8) >> 0x18) as i32 - 1;
                if i_var3 < 0 {
                    i_var3 = 9;
                }
                cp = u32::from(b_var1) / 10 * i_var3 as u32 + u32::from(b_var1);
            }
        }

        cp
    }

    pub fn get_mp(self) -> u32 {
        if self.id < 99 {
            let base_mp = (self.get_cp() / 3) * 2;

            if self.is_premium {
                base_mp + 10
            } else {
                base_mp
            }
        } else {
            0
        }
    }

    pub fn name(self) -> &'static str {
        if (self.id as usize) < CARD_DATA_TABLE.len() {
            Some(CARD_DATA_TABLE[self.id as usize].name)
        } else {
            None
        }.expect("Invalid card id!")
    }

    pub fn category(self) -> u8 {
        if (self.id as usize) < CARD_DATA_TABLE.len() {
            Some(CARD_DATA_TABLE[self.id as usize].category)
        } else {
            None
        }.expect("Invalid card id!")
    }

    fn should_display_value(self) -> bool {
        CardCategory::try_from(self.category()).map(|c| {
            match c {
                CardCategory::PrizeOrb |
                    CardCategory::StoryCard |
                    CardCategory::SpecialRoom |
                    CardCategory::BossCard |
                    CardCategory::EnemyCard |
                    CardCategory::WorldCard => false,
                _ => true
            }
        }).unwrap_or(false)
    }

    fn should_display_premium(self) -> bool {
        CardCategory::try_from(self.category()).map(|c| {
            match c {
                CardCategory::AttackCard |
                    CardCategory::MagicCard |
                    CardCategory::ItemCard |
                    CardCategory::SummonCard => true,
                _ => false
            }
        }).unwrap_or(false)
    }
}

impl fmt::Debug for Card {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Card {{ category: 0x{:02X}, value: 0x{:02X}, is_premium: {}, id: 0x{:02X} }}", self.category(), self.value, self.is_premium, self.id)
    }
}

impl fmt::Display for Card {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if self.is_premium && self.should_display_premium() {
            write!(f, "Premium ")?;
        }

        write!(f, "{}", self.name())?;

        if self.should_display_value() {
            write!(f, " {}", self.value)?;
        }

        Ok(())
    }
}

impl From<u32> for Card {
    fn from(card: u32) -> Self {
        let premium = (card >> 8) & 0x1 == 1;
        Card::new(
            ((card >> (2 * 8)) & 0xff) as u8,
            premium,
            (card & 0xff) as u8
        )
    }
}

impl From<Card> for u32 {
    fn from(card: Card) -> u32 {
        let p = if card.is_premium { 1 } else { 0 };

        (u32::from(card.category())) << (3 * 8) |
            (u32::from(card.value)) << (2 * 8) |
            p << 8 |
            u32::from(card.id)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const CARDS: &'static [Card] = &[
        Card::new(6, false, 0x01), // Kingdom Key 6
        Card::new(1, true, 0x02), // Premium Three Wishes 1
        Card::new(4, false, 0x0D), // Oathkeeper 4
        Card::new(9, false, 0x02)  // Three Wishes 9
    ];

    const EXPECTED: &'static [u32] = &[
        15, 15, 31, 23
    ];

    #[test]
    fn test_card_get_cp() {
        assert_eq!(CARDS.len(), EXPECTED.len(), "Length of inputs and outputs for test must be of same length!");

        for i in 0..CARDS.len() {
            assert_eq!(CARDS[i].get_cp(), EXPECTED[i], "Failed to compute proper cp!");
        }
    }
}
