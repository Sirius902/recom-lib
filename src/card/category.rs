use std::convert::TryFrom;

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum CardCategory {
    AttackCard,
    MagicCard,
    SummonCard,
    ItemCard,
    FriendCard,
    /// I'm not sure if this is actually used. It seems boss cards are in the `EnemyCard` category.
    BossCard,
    EnemyCard,
    RedMapCard,
    GreenMapCard,
    BlueMapCard,
    StoryCard,
    WorldCard,
    PrizeOrb,
    SpecialRoom
}

impl TryFrom<u8> for CardCategory {
    type Error = &'static str;

    fn try_from(category_id: u8) -> Result<Self, Self::Error> {
        match category_id {
            0x11 => Ok(CardCategory::AttackCard),
            0x22 => Ok(CardCategory::MagicCard),
            0x33 => Ok(CardCategory::SummonCard),
            0x44 => Ok(CardCategory::ItemCard),
            0x55 => Ok(CardCategory::FriendCard),
            0x66 => Ok(CardCategory::BossCard),
            0x77 => Ok(CardCategory::EnemyCard),
            0x99 => Ok(CardCategory::RedMapCard),
            0xA9 => Ok(CardCategory::GreenMapCard),
            0xB9 => Ok(CardCategory::BlueMapCard),
            0xCA => Ok(CardCategory::StoryCard),
            0xDB => Ok(CardCategory::WorldCard),
            0x0D => Ok(CardCategory::PrizeOrb),
            0x0C => Ok(CardCategory::SpecialRoom),
            _ => Err("Invalid or unsupported category id.")
        }
    }
}

impl From<CardCategory> for u8 {
    fn from(cat: CardCategory) -> u8 {
        match cat {
            CardCategory::AttackCard => 0x11,
            CardCategory::MagicCard => 0x22,
            CardCategory::SummonCard => 0x33,
            CardCategory::ItemCard => 0x44,
            CardCategory::FriendCard => 0x55,
            CardCategory::BossCard => 0x66,
            CardCategory::EnemyCard => 0x77,
            CardCategory::RedMapCard => 0x99,
            CardCategory::GreenMapCard => 0xA9,
            CardCategory::BlueMapCard => 0xB9,
            CardCategory::StoryCard => 0xCA,
            CardCategory::WorldCard => 0xDB,
            CardCategory::PrizeOrb => 0x0D,
            CardCategory::SpecialRoom => 0x0C
        }
    }
}
