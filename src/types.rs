/// The unique id for an entity type.
/// 
/// For example Save Points have the id `0x14` and barrels from Traverse Town have id `0x85`.
pub type EntityId = u16;
