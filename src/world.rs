use std::fmt;

use strum_macros::EnumIter;
use crate::resource::load_gim_prob;
use crate::types::EntityId;

#[derive(Copy, Clone, PartialEq, Eq, EnumIter)]
pub enum World {
    TraverseTown,
    Agrabah,
    HalloweenTown,
    Monstro,
    OlympusColiseum,
    Wonderland,
    Atlantica,
    AcreWood,
    Neverland,
    HollowBastion,
    TwilightTown,
    DestinyIslands,
    CastleOblivion
}

impl World {
    pub fn dir_name(self) -> &'static str {
        match self {
            Self::TraverseTown => "tr",
            Self::Agrabah => "ag",
            Self::HalloweenTown => "ha",
            Self::Monstro => "mo",
            Self::OlympusColiseum => "ol",
            Self::Wonderland => "wo",
            Self::Atlantica => "at",
            Self::AcreWood => "po",
            Self::Neverland => "ne",
            Self::HollowBastion => "ho",
            Self::TwilightTown => "tw",
            Self::DestinyIslands => "de",
            Self::CastleOblivion => "ca"
        }
    }

    pub fn gimmicks(self) -> Vec<EntityId> {
        load_gim_prob(self).iter().map(|(k, _)| *k).collect::<Vec<_>>()
    }
}

impl fmt::Display for World {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let display = match *self {
            Self::TraverseTown => "Traverse Town",
            Self::Agrabah => "Agrabah",
            Self::HalloweenTown => "Halloween Town",
            Self::Monstro => "Monstro",
            Self::OlympusColiseum => "Olympus Coliseum",
            Self::Wonderland => "Wonderland",
            Self::Atlantica => "Atlantica",
            Self::AcreWood => "100 Acre Wood",
            Self::Neverland => "Neverland",
            Self::HollowBastion => "Hollow Bastion",
            Self::TwilightTown => "Twilight Town",
            Self::DestinyIslands => "Destiny Islands",
            Self::CastleOblivion => "Castle Oblivion"
        };

        write!(f, "{}", display)
    }
}

#[derive(Copy, Clone, PartialEq, Eq, Hash, EnumIter)]
pub enum Room {
    UnknownRoom,
    ConquerorsRespite,
    RoomOfRewards,
    // Red Map Cards
    TranquilDarkness,
    TeemingDarkness,
    FeebleDarkness,
    AlmightyDarkness,
    SleepingDarkness,
    LoomingDarkness,
    PremiumRoom,
    WhiteRoom,
    BlackRoom,
    BottomlessDarkness,
    RouletteRoom,
    // Green Map Cards
    MartialWalking,
    SorcerousWaking,
    AlchemicWaking,
    MeetingGround,
    StagnantSpace,
    StrongInitiative,
    LastingDaze,
    // Blue Map Cards
    CalmBounty,
    GuardedTrove,
    FalseBounty,
    MomentsReprieve,
    MinglingWorlds,
    MoogleRoom
}

impl Room {
    pub fn scene_number(self) -> i16 {
        match self {
            Self::UnknownRoom => 0x0001,
            Self::ConquerorsRespite => 0x0002,
            Self::RoomOfRewards => 0x0018,
            // Red Map Cards
            Self::TranquilDarkness => 0x0016,
            Self::TeemingDarkness => 0x0003,
            Self::FeebleDarkness => 0x0012,
            Self::AlmightyDarkness => 0x0010,
            Self::LoomingDarkness => 0x0013,
            Self::PremiumRoom => 0x0011,
            Self::WhiteRoom => 0x0014,
            Self::BlackRoom => 0x000C,
            Self::SleepingDarkness => 0x000D,
            Self::BottomlessDarkness => 0x001A,
            Self::RouletteRoom => 0x0019,
            // Green Map Cards
            Self::MartialWalking => 0x0015,
            Self::MeetingGround => 0x000B,
            Self::StagnantSpace => 0x0009,
            Self::StrongInitiative => 0x000E,
            Self::SorcerousWaking => 0x000F,
            Self::AlchemicWaking => 0x000A,
            Self::LastingDaze => 0x0017,
            // Blue Map Cards
            Self::CalmBounty => 0x0005,
            Self::GuardedTrove => 0x0007,
            Self::FalseBounty => 0x0008,
            Self::MomentsReprieve => 0x0006,
            Self::MinglingWorlds => 0x0012,
            Self::MoogleRoom => 0x0004,
        }
    }
}

impl fmt::Display for Room {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let display = match *self {
            Self::UnknownRoom => "Unknown Room",
            Self::ConquerorsRespite => "Conqueror's Respite",
            Self::RoomOfRewards => "Room Of Rewards",
            // Red Map Cards
            Self::TranquilDarkness => "Tranquil Darkness",
            Self::TeemingDarkness => "Teeming Darkness",
            Self::FeebleDarkness => "Feeble Darkness",
            Self::AlmightyDarkness => "Almighty Darkness",
            Self::SleepingDarkness => "Sleeping Darkness",
            Self::LoomingDarkness => "Looming Darkness",
            Self::PremiumRoom => "Premium Room",
            Self::WhiteRoom => "White Room",
            Self::BlackRoom => "Black Room",
            Self::BottomlessDarkness => "Bottomless Darkness",
            Self::RouletteRoom => "Roulette Room",
            // Green Map Cards
            Self::MartialWalking => "Martial Walking",
            Self::SorcerousWaking => "Sorcerous Waking",
            Self::AlchemicWaking => "Alchemic Waking",
            Self::MeetingGround => "Meeting Ground",
            Self::StagnantSpace => "Stagnant Space",
            Self::StrongInitiative => "Strong Initiative",
            Self::LastingDaze => "Lasting Daze",
            // Blue Map Cards
            Self::CalmBounty => "Calm Bounty",
            Self::GuardedTrove => "Guarded Trove",
            Self::FalseBounty => "False Bounty",
            Self::MomentsReprieve => "Moment's Reprieve",
            Self::MinglingWorlds => "Mingling Worlds",
            Self::MoogleRoom => "Moogle Room"
        };

        write!(f, "{}", display)
    }
}
