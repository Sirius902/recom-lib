use std::fmt;

use super::card::Card;

#[derive(Eq, PartialEq, Debug, Copy, Clone)]
pub enum OrbSize {
    Small,
    Large
}

impl fmt::Display for OrbSize {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let display = match self {
            Self::Small => "Small",
            Self::Large => "Large"
        };

        write!(f, "{}", display)
    }
}

#[derive(Eq, PartialEq, Debug, Copy, Clone)]
pub enum Orbs {
    HP {
        size: OrbSize,
        amount: usize
    },
    MP {
        size: OrbSize,
        amount: usize
    }
}

impl Orbs {
    pub fn value(self) -> usize {
        match self {
            Self::MP { size: OrbSize::Small, amount } => 4 * amount,
            Self::MP { size: OrbSize::Large, amount } => 10 * amount,
            Self::HP { size: OrbSize::Small, amount } => 2 * amount,
            Self::HP { size: OrbSize::Large, amount } => 4 * amount,
        }
    }
}

#[derive(Eq, PartialEq, Debug, Clone)]
pub enum Prize {
    PrizeOrbs(Orbs),
    Card(Card),
    /// This means an enemy will be spawned as a result, i.e. Barrel Spiders.
    Mimic
}

impl fmt::Display for Prize {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let display = match self {
            Self::PrizeOrbs(orbs) => {
                let value = orbs.value();

                match orbs {
                    Orbs::HP { size, amount } =>
                        format!("{} {} HP Orbs (+{} HP)", amount, size, value),
                    Orbs::MP { size, amount } =>
                        format!("{} {} MP Orbs (+{} MP)", amount, size, value)
                }
            },
            Self::Card(card) => card.to_string(),
            Self::Mimic => "Barrel Spider".to_owned()
        };

        write!(f, "{}", display)
    }
}
