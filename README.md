# Recom Lib
## About
This project is a simulation of the random number generator from Kingdom Hearts
Re: Chain of Memories in the HD 1.5 Remix that allows for the prediction of
items that depend on the random number generator.

## Note
Note that the methods for predicting these drops were derived from a modified
version of the PS2 release using the same seed for the rng as is used in the
1.5 Remix release. This means that cards added by 1.5 Remix will not be
supported like the 358/2 Days cards. If you have obtained any of these cards in
a save file there is a chance that you could get incorrect results from using
the library. The reason the RNG is predictable is because when programming
1.5 Remix, the devs ended up seeding the RNG with the same value every time the
game is started unlike the PS2 release which was correctly seeded with
different values. This means that this library will not work with the PS2
releases of the game. Also, <b>this is important</b>, for every frame an active
enemy is loaded (after their spawning animation) the RNG advances rapidly and
becomes unpredictable unless the enemy is asleep. So this library will not be
able to predict any encounter with active enemies. If you notice any
inconsistencies, excluding 1.5 Remix exclusive cards, with this library and the game, let me know.

# Roadmap
Currently, only prize orbs, cards, and barrel spiders are supported for drops
and I plan to add support for Moogle Shops, Map Card drops from enemies, and
possibly chests.

Also, world and room changes are planned as actions.

Another thing is not all the objects that drop items are implemented yet but I
plan on adding them. It's just time consuming to collect all the ids.

# Usage
To add this library to a project add this line to the Cargo.toml file:
```
recom-lib = { git = "https://gitlab.com/Sirius902/recom-lib" }
```

## Sample Program
To follow the fist part of the RNG Manipulation route in the Moment's Reprieve room
found by Sonicshadowsilver2 for Traverse Town, use the below sample program.
Video here: https://www.youtube.com/watch?v=HfdVrwuYDao

```rust
use recom_lib::{
    world::{World, Room},
    gim::{UnlockableCard, Gimmick},
    predictor::{Predictor, Action, Character}
};

fn main() {
    let mut pred = Predictor::new(
        World::TraverseTown,
        Room::MomentsReprieve,
        UnlockableCard::none(),
        Character::Sora
    );

    // A list of actions to simulate
    let actions = vec![
        Action::Roll { count: 6 },                    // Roll six times
        Action::Break {                               // Attack orange lamp on wall with one hit
            gimmick_id: Gimmick::TROrangeLamp.id(),
            hit_count: 1
        },
        Action::Roll { count: 7 },                    // Roll seven times
        Action::Break {                               // Attack crate with one attack
            gimmick_id: Gimmick::TRCrate.id(),
            hit_count: 1
        },
        Action::Break {                               // Attack other crate, but since the attack was from the previous Break action, pass a zero
            gimmick_id: Gimmick::TRCrate.id(),
            hit_count: 0
        },
        Action::Roll { count: 3 },                    // Roll three times
        Action::Break {                               // Attack orange lamp on wall with one hit
            gimmick_id: Gimmick::TROrangeLamp.id(),
            hit_count: 1
        },
        Action::Roll { count: 4 },                    // Roll four times
        Action::Break {                               // Attack lamp post with one attack
            gimmick_id: Gimmick::TRLampPost.id(),
            hit_count: 1
        },
    ];

    let results = pred.predict_all(&actions);

    for prize in results {
        if let Some(prize) = prize {
            println!("{}", prize);
        }
    }
}
```

The program above has the following output which matches the video:

```
3 Small MP Orbs (+12 MP)
5 Large MP Orbs (+50 MP)
5 Small MP Orbs (+20 MP)
5 Large MP Orbs (+50 MP)
Blizzard 3
```